///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/

// KinematicFitTool.cxx
// Source file for class KinematicFitTool
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
/////////////////////////////////////////////////////////////////// 


// KinematicFitTool includes
#include <AsgTools/MessageCheck.h>

#include "KinematicFitTool/KinematicFitTool.h"
#include "PathResolver/PathResolver.h"

#include "xAODJet/Jet.h"
#include "xAODEgamma/Photon.h"
#include "TLorentzVector.h"

KinematicFitTool::KinematicFitTool(const std::string& name) : asg::AsgTool (name), m_JetAlgo(""), m_BtaggingWP(), m_Jet_Min_Pt(20.), m_photon_Res(0.01), m_angles_Res(0.01), m_isFixAngles(true), m_file_name("")
{ 
	declareProperty( "Jet Collection", m_JetAlgo = "AntiKt4EMPFlow" );
	declareProperty( "Jet Min Pt", m_Jet_Min_Pt = 20. );
	declareProperty( "BTagging WP", m_BtaggingWP = "MV2c10_FixedCutBEff_70" );
	declareProperty( "Photon Resolution", m_photon_Res = .01 );
	declareProperty( "Angles Resolution", m_angles_Res = .01 );
	declareProperty( "Fix Angles Fit", m_isFixAngles = true );
	declareProperty( "Files Name", m_file_name = "_TaOgataParameters.root" );
}

KinematicFitTool::~KinematicFitTool() {


}


StatusCode KinematicFitTool::initialize() {
	
  ANA_MSG_INFO ("Initializing KinematicFit ... " << name() << " ... "); 
  ANA_CHECK( this->initializeTool( name() ) );
  return StatusCode::SUCCESS;
}

StatusCode KinematicFitTool::finalize() {

  return StatusCode::SUCCESS;
}


StatusCode KinematicFitTool::initializeTool(const std::string& name) {

  if(m_JetAlgo == "")
  {
	ANA_MSG_FATAL("KinematicFitTool::initialize() : Please set JetCollection"); 
	return StatusCode::FAILURE;
  }

  const std::string m_EnResp = PathResolverFindCalibFile("KinematicFitTool/E_"+m_JetAlgo+m_file_name);
  const std::string m_PtResp = PathResolverFindCalibFile("KinematicFitTool/pT_"+m_JetAlgo+m_file_name);
  const std::string m_PhotResp = PathResolverFindCalibFile("KinematicFitTool/PhotRes_"+m_JetAlgo+"_TaOTaParameters.root");
  const std::string m_pXconstr   = PathResolverFindCalibFile("KinematicFitTool/pXconstr_"+m_JetAlgo+"_TaOTaParameters.root");
  const std::string m_pYconstr   = PathResolverFindCalibFile("KinematicFitTool/pYconstr_"+m_JetAlgo+"_TaOTaParameters.root");

  fResolution = new KinematicFitResolution();
 	ANA_CHECK(fResolution->setProperty("Photon Resolution", m_photon_Res));
	ANA_CHECK(fResolution->setProperty("Angles Resolution", m_angles_Res));
	ANA_CHECK(fResolution->setProperty("Fix Angles Fit", m_isFixAngles));
        ANA_CHECK(fResolution->setProperty("EnergyResponseFile", m_EnResp));
        ANA_CHECK(fResolution->setProperty("PtResponseFile", m_PtResp));
        ANA_CHECK(fResolution->setProperty("PhotonResponseFile", m_PhotResp));
        ANA_CHECK(fResolution->setProperty("pXconstraintFile", m_pXconstr));
        ANA_CHECK(fResolution->setProperty("pYconstraintFile", m_pYconstr));

	ANA_CHECK(fResolution->initialize());

 fEvent = new KinematicFitEvent();
	ANA_CHECK(fEvent->setProperty("Jet Min Pt", m_Jet_Min_Pt)); 
	ANA_CHECK(fEvent->setProperty("BTagging WP", m_BtaggingWP));
	ANA_CHECK(fEvent->initialize());

  ANA_MSG_INFO("KinematicFitTool::initialize() : " << name << " is initialized!");

  ANA_MSG_INFO("KinematicFitTool::initialize() : KinematicFitRun is Ready!");
  return StatusCode::SUCCESS;
}


StatusCode KinematicFitTool::applyKF(xAOD::PhotonContainer& photons, xAOD::JetContainer& jets, Double_t& KF1_Mbb) 
{

	static SG::AuxElement::Decorator<Float_t> PT("KF_PT");	
	static SG::AuxElement::Decorator<Float_t> ETA("KF_ETA");	
	static SG::AuxElement::Decorator<Float_t> PHI("KF_PHI");	
	static SG::AuxElement::Decorator<Float_t> M("KF_M");	
	static SG::AuxElement::Decorator<Char_t> isB("KF_isB");	

	for( auto jet : jets)
	{
		PT(*jet)    = jet->pt();
		ETA(*jet)   = jet->eta();
		PHI(*jet)   = jet->phi();
		M(*jet)     = jet->m();
		isB(*jet)   = false;
	}
			
	for( auto ph : photons)
	{
		PT(*ph)    = ph->pt();
		ETA(*ph)   = ph->eta();
		PHI(*ph)   = ph->phi();
	}

	// --- For the Kinematic Fit Second run per event --- //
	std::vector< TLorentzVector > jetsCopy(jets.size());
        for(unsigned int i = 0; i < jetsCopy.size(); i++){
          jetsCopy[i].SetPtEtaPhiE(jets[i]->pt(), jets[i]->eta(), jets[i]->phi(), jets[i]->e());
        }
	std::vector< TLorentzVector > photCopy(photons.size());
        for(unsigned int i = 0; i < photCopy.size(); i++){
          photCopy[i].SetPtEtaPhiE(photons[i]->pt(), photons[i]->eta(), photons[i]->phi(), photons[i]->e());
	}

        for(int i = 1 ; i <= 2; i++){
          if(i == 2){

            for( auto jet : jets)
              {
                PT(*jet)    = jetsCopy[i].Pt();
                ETA(*jet)   = jetsCopy[i].Eta();
                PHI(*jet)   = jetsCopy[i].Phi();
                M(*jet)     = jetsCopy[i].M();
                isB(*jet)   = false;
              }

            for( auto ph : photons)
              {
                PT(*ph)    = photCopy[i].Pt();
                ETA(*ph)   = photCopy[i].Eta();
                PHI(*ph)   = photCopy[i].Phi();
              }

          }
	  if(!fEvent->applySelection(photons, jets)) return StatusCode::SUCCESS;
	  fProcessor = new KinematicFitRun();
	  ANA_CHECK(fProcessor->setProperty("IncludingConstr",i));
	  fProcessor->RunKF(fEvent, fResolution);
	  DecorateEvent(fEvent, fProcessor, KF1_Mbb, i);
	  fEvent->Clear();
	  delete fProcessor;
	}

  return StatusCode::SUCCESS; 
}

void KinematicFitTool::DecorateEvent(KinematicFitEvent*& Event, KinematicFitRun*& Run, Double_t& KF1_Mbb, int i)
{
	static SG::AuxElement::Decorator<Float_t> PT("KF_PT");	
	static SG::AuxElement::Decorator<Float_t> ETA("KF_ETA");	
	static SG::AuxElement::Decorator<Float_t> PHI("KF_PHI");	
	static SG::AuxElement::Decorator<Float_t> M("KF_M");	
	static SG::AuxElement::Decorator<Char_t> isB("KF_isB");	

	bool isFitted = Run->isFitted();
	if(isFitted)
	{

		TLorentzVector jet1;
		TLorentzVector jet2;

		jet1.SetPtEtaPhiE((Event->GetFitBJetOne()).Pt(),(Event->GetFitBJetOne()).Eta(),(Event->GetFitBJetOne()).Phi(),(Event->GetFitBJetOne()).E());
		jet2.SetPtEtaPhiE((Event->GetFitBJetTwo()).Pt(),(Event->GetFitBJetTwo()).Eta(),(Event->GetFitBJetTwo()).Phi(),(Event->GetFitBJetTwo()).E());

                TLorentzVector photon1;
                TLorentzVector photon2;

                photon1.SetPtEtaPhiE((Event->GetFitPhotonOne()).Pt(),(Event->GetFitPhotonOne()).Eta(),(Event->GetFitPhotonOne()).Phi(),(Event->GetFitPhotonOne()).E());
                photon2.SetPtEtaPhiE((Event->GetFitPhotonTwo()).Pt(),(Event->GetFitPhotonTwo()).Eta(),(Event->GetFitPhotonTwo()).Phi(),(Event->GetFitPhotonTwo()).E());

                TLorentzVector jj = jet1 + jet2;

                if(i == 1){
                  KF1_Mbb = jj.M();
                }else if(i == 2){
		  xAOD::JetFourMom_t jet14vec(jet1.Pt(),jet1.Eta(),jet1.Phi(),jet1.M());
		  xAOD::JetFourMom_t jet24vec(jet2.Pt(),jet2.Eta(),jet2.Phi(),jet2.M());

		  (Event->GetBJetOne())->setJetP4(jet14vec);
		  (Event->GetBJetTwo())->setJetP4(jet24vec);

		  PT(*(Event->GetBJetOne()))    = Event->GetFitBJetOne().Pt();
		  ETA(*(Event->GetBJetOne()))   = Event->GetFitBJetOne().Eta();
		  PHI(*(Event->GetBJetOne()))   = Event->GetFitBJetOne().Phi();
		  M(*(Event->GetBJetOne()))     = Event->GetFitBJetOne().M();
		  PT(*(Event->GetBJetTwo()))    = Event->GetFitBJetTwo().Pt();
		  ETA(*(Event->GetBJetTwo()))   = Event->GetFitBJetTwo().Eta();
		  PHI(*(Event->GetBJetTwo()))   = Event->GetFitBJetTwo().Phi();
		  M(*(Event->GetBJetTwo()))     = Event->GetFitBJetTwo().M();
		  isB(*(Event->GetBJetTwo()))   = true;
		  isB(*(Event->GetBJetOne()))   = true;

		  (Event->GetPhotonOne())->setP4(photon1.Pt(),photon1.Eta(),photon1.Phi(),photon1.M());
		  (Event->GetPhotonTwo())->setP4(photon2.Pt(),photon2.Eta(),photon2.Phi(),photon2.M());

		  PT(*(Event->GetPhotonOne()))  = Event->GetFitPhotonOne().Pt();
		  ETA(*(Event->GetPhotonOne())) = Event->GetFitPhotonOne().Eta();
		  PHI(*(Event->GetPhotonOne())) = Event->GetFitPhotonOne().Phi();
		  PT(*(Event->GetPhotonTwo()))  = Event->GetFitPhotonTwo().Pt();
		  ETA(*(Event->GetPhotonTwo())) = Event->GetFitPhotonTwo().Eta();
		  PHI(*(Event->GetPhotonTwo())) = Event->GetFitPhotonTwo().Phi();
		
		  if(Event->GetAddJets().size() >= FitType::ThreeJet)
		    {

		      std::vector< TLorentzVector > jets(Event->GetAddJets().size());

		      for(unsigned int i =0; i<Event->GetAddJets().size(); i++){
			jets[i].SetPtEtaPhiE((Event->GetFitAddJet())[i].Pt(),(Event->GetFitAddJet())[i].Eta(),(Event->GetFitAddJet())[i].Phi(),(Event->GetFitAddJet())[i].E());

			xAOD::JetFourMom_t jet4vec(jets[i].Pt(),jets[i].Eta(),jets[i].Phi(),jets[i].M());

			(Event->GetAddJets().at(i))->setJetP4(jet4vec);

			PT(*(Event->GetAddJets().at(i)))  = (Event->GetFitAddJet())[i].Pt();
			ETA(*(Event->GetAddJets().at(i))) = (Event->GetFitAddJet())[i].Eta();
			PHI(*(Event->GetAddJets().at(i))) = (Event->GetFitAddJet())[i].Phi();
			M(*(Event->GetAddJets().at(i)))   = (Event->GetFitAddJet())[i].M();
			isB(*(Event->GetAddJets().at(i))) = false;
		      }
		    }
		  }

	}else{

		TLorentzVector jet1;
		TLorentzVector jet2;

		jet1.SetPtEtaPhiE((Event->GetBJetOne())->pt(),(Event->GetBJetOne())->eta(),(Event->GetBJetOne())->phi(),(Event->GetBJetOne())->e());
		jet2.SetPtEtaPhiE((Event->GetBJetTwo())->pt(),(Event->GetBJetTwo())->eta(),(Event->GetBJetTwo())->phi(),(Event->GetBJetTwo())->e());

                TLorentzVector photon1;
                TLorentzVector photon2;

                photon1.SetPtEtaPhiE((Event->GetPhotonOne())->pt(),(Event->GetPhotonOne())->eta(),(Event->GetPhotonOne())->phi(),(Event->GetPhotonOne())->e());
                photon2.SetPtEtaPhiE((Event->GetPhotonTwo())->pt(),(Event->GetPhotonTwo())->eta(),(Event->GetPhotonTwo())->phi(),(Event->GetPhotonTwo())->e());


                TLorentzVector jj = jet1 + jet2;

                if(i == 1){
                  KF1_Mbb = jj.M();
                }else if(i == 2){
		  xAOD::JetFourMom_t jet14vec(jet1.Pt(),jet1.Eta(),jet1.Phi(),jet1.M());
		  xAOD::JetFourMom_t jet24vec(jet2.Pt(),jet2.Eta(),jet2.Phi(),jet2.M());

		  (Event->GetBJetOne())->setJetP4(jet14vec);
		  (Event->GetBJetTwo())->setJetP4(jet24vec);

		  PT(*(Event->GetBJetOne()))  = Event->GetBJetOne()->pt();
		  ETA(*(Event->GetBJetOne())) = Event->GetBJetOne()->eta();
		  PHI(*(Event->GetBJetOne())) = Event->GetBJetOne()->phi();
		  M(*(Event->GetBJetOne()))   = Event->GetBJetOne()->m();
		  PT(*(Event->GetBJetTwo()))  = Event->GetBJetTwo()->pt();
		  ETA(*(Event->GetBJetTwo())) = Event->GetBJetTwo()->eta();
		  PHI(*(Event->GetBJetTwo())) = Event->GetBJetTwo()->phi();
		  M(*(Event->GetBJetTwo()))   = Event->GetBJetTwo()->m();

		  (Event->GetPhotonOne())->setP4(photon1.Pt(),photon1.Eta(),photon1.Phi(),photon1.M());
		  (Event->GetPhotonTwo())->setP4(photon2.Pt(),photon2.Eta(),photon2.Phi(),photon2.M());

		  PT(*(Event->GetPhotonOne()))  = Event->GetPhotonOne()->pt();
		  ETA(*(Event->GetPhotonOne())) = Event->GetPhotonOne()->eta();
		  PHI(*(Event->GetPhotonOne())) = Event->GetPhotonOne()->phi();
		  PT(*(Event->GetPhotonTwo()))  = Event->GetPhotonTwo()->pt();
		  ETA(*(Event->GetPhotonTwo())) = Event->GetPhotonTwo()->eta();
		  PHI(*(Event->GetPhotonTwo())) = Event->GetPhotonTwo()->phi();

		  if(Event->GetAddJets().size() >= FitType::ThreeJet)
		    {

		      std::vector< TLorentzVector > jets(Event->GetAddJets().size());

		      for(unsigned int i =0; i<Event->GetAddJets().size(); i++){
			jets[i].SetPtEtaPhiE((Event->GetAddJets().at(i))->pt(),(Event->GetAddJets().at(i))->eta(),(Event->GetAddJets().at(i))->phi(),(Event->GetAddJets().at(i))->e());

			xAOD::JetFourMom_t jet4vec(jets[i].Pt(),jets[i].Eta(),jets[i].Phi(),jets[i].M());

			(Event->GetAddJets().at(i))->setJetP4(jet4vec);

			PT(*(Event->GetAddJets().at(i)))  = (Event->GetAddJets().at(i))->pt();
			ETA(*(Event->GetAddJets().at(i))) = (Event->GetAddJets().at(i))->eta();
			PHI(*(Event->GetAddJets().at(i))) = (Event->GetAddJets().at(i))->phi();
			M(*(Event->GetAddJets().at(i)))   = (Event->GetAddJets().at(i))->m();
		      }
		    }
		}
	}
}
