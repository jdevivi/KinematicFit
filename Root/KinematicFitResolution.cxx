///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/

// KinematicFitResolution.cxx
// Source file for class KinematicFitResolution
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
/////////////////////////////////////////////////////////////////// 


// KinematicFitResolution includes
#include <AsgTools/MessageCheck.h>
#include "KinematicFitTool/KinematicFitResolution.h"

KinematicFitResolution::KinematicFitResolution() : asg::AsgTool ("KinematicFitResolution"), m_photon_Res(0), m_angles_Res(0), m_EnergyResponseName(""), m_PtResponseName(""), m_PhotonResponseName(""), m_pXconstraintName(""),m_pYconstraintName("")
{
	declareProperty( "Photon Resolution", m_photon_Res);
	declareProperty( "Angles Resolution", m_angles_Res);
	declareProperty( "Fix Angles Fit", m_isFixAngles);
	declareProperty( "EnergyResponseFile", m_EnergyResponseName = "");
	declareProperty( "PtResponseFile", m_PtResponseName = "" );
	declareProperty("PhotonResponseFile", m_PhotonResponseName = "" );
	declareProperty("pXconstraintFile",m_pXconstraintName = "" );
	declareProperty("pYconstraintFile",m_pYconstraintName = "" );
}

KinematicFitResolution::~KinematicFitResolution() {

}

StatusCode KinematicFitResolution::initialize()
{

	if(m_photon_Res == 0) return StatusCode::FAILURE;
	if(m_angles_Res == 0) return StatusCode::FAILURE;
        if(m_EnergyResponseName == "") return StatusCode::FAILURE;
        if(m_PtResponseName == "") return StatusCode::FAILURE;
        if(m_PhotonResponseName == "") return StatusCode::FAILURE;
        if(m_pXconstraintName == "") return StatusCode::FAILURE;
        if(m_pYconstraintName == "") return StatusCode::FAILURE;

        m_EnergyResponse = new TFile(m_EnergyResponseName, "READ");
        m_PtResponse = new TFile(m_PtResponseName, "READ");
        m_PhotonResolution = new TFile(m_PhotonResponseName, "READ");
        m_pXconstraint = new TFile(m_pXconstraintName, "READ");
        m_pYconstraint = new TFile(m_pYconstraintName, "READ");

        import_par_tensor();

        m_EnergyResponse->Close();
        m_PtResponse->Close();
        m_pXconstraint->Close();
        m_pYconstraint->Close();
        m_PhotonResolution->Close();

	ANA_MSG_INFO("KinematicFitResolution::initialize() : KinematicFitResolution is initialized!");
	return StatusCode::SUCCESS;
}

StatusCode KinematicFitResolution::finalize() {

  return StatusCode::SUCCESS;
}

double KinematicFitResolution::PhotonRes()
{

	return m_photon_Res;
}

double KinematicFitResolution::AnglesRes()
{

	return m_angles_Res;
}

bool KinematicFitResolution::FixAngles()
{
	return m_isFixAngles;
}

double KinematicFitResolution::TaOgataResponse(TLorentzVector FitJet,  xAOD::Jet* Jet,const std::string& EorPT){

  double TFValue=0.0, Result=0.0;
  int tensor_row = -99, tensor_col = 0;

  if(EorPT == "E"){
    TFValue = (FitJet.E()-Jet->e())/FitJet.E();
  }else if(EorPT == "pT"){
    TFValue = (FitJet.Pt()-Jet->pt())/FitJet.Pt();
  }

  //--- Detector --- //
  std::string DetectPart = "";
  if( (FitJet.Eta()>=-4.4 && FitJet.Eta()<-2.5) || (FitJet.Eta()>=2.5 && FitJet.Eta()<4.4) ){
    DetectPart = "NoTrack";
    tensor_row=3;
  }else if( (FitJet.Eta()>=-2.5 && FitJet.Eta()<-1.52) || (FitJet.Eta()>=1.52 && FitJet.Eta()<2.5) ){
    DetectPart = "Endcap";
    tensor_row=2;
  }else if((FitJet.Eta()>=-1.52 && FitJet.Eta()<-1.37) || (FitJet.Eta()>=1.37 && FitJet.Eta()<1.52) ){
    DetectPart = "Crack";
    tensor_row=1;
  }else if(FitJet.Eta()>=-1.37 && FitJet.Eta()<1.37){
    DetectPart = "Barrel";
    tensor_row=0;
  }

  double ln_pT = log(FitJet.Pt()/1000);

  float logPT_bin [] = {2.,3.7,4.,4.5,5.,5.3,6.};
  int nbins = 7;
  int bin = -99;
  for(int i = 0; i<nbins; i++)
    {
      if(ln_pT >= logPT_bin[i] && ln_pT < logPT_bin[i+1])
        {
          bin = i;
          break;
        }
    }
  if(ln_pT > 6.)
    {
      bin = nbins - 2;
    }
  if(ln_pT < 2.)
    {
      bin = 0;
    }

  tensor_col=bin;

  if(EorPT == "E"){
    Result = taOgataRF(TFValue,taOgata_par_tensor_E[tensor_row][tensor_col]);
  }else if(EorPT == "pT"){
    Result = taOgataRF(TFValue,taOgata_par_tensor_pT[tensor_row][tensor_col]);
  }

  return Result;
}

double KinematicFitResolution::MomConstraint(double FitJetp,  int nAddJets,const std::string& PxOrPy){

  double Res_constr = 0.0;

  if(PxOrPy == "pX"){
    Res_constr = TaOTa(FitJetp, TaOTa_par_tensor_pXconstr[nAddJets]);
  }else if(PxOrPy == "pY"){
    Res_constr = TaOTa(FitJetp, TaOTa_par_tensor_pYconstr[nAddJets]);
  }
  return Res_constr;
}

double KinematicFitResolution::Phot_Resolution(double x)
{
  //it's trivial but implemented in this way for possible future changes
  return TaOTa(x, TaoTa_par_PhotonResol);
}

void KinematicFitResolution::import_par_tensor(){
  const char* DetectPart[4] = {"Barrel", "Crack", "Endcap", "NoTrack"};
  std::vector<float> logPT = {2.0,3.7,4.0,4.5,5.0,5.3,6.0};

  // ------------- Parameter tensors TF inizialization ----------------- //
  for (int i=0; i<4; i++){
    for (int j=0; j<6; j++){
      taOgata_par_tensor_E[i][j] = *(std::vector<double>*)(m_EnergyResponse->Get(Form("TransferFunction_%s_%1.1f_%1.1f_ln(pT[GeV])", DetectPart[i], logPT[j],logPT[j+1])));
      taOgata_par_tensor_pT[i][j] = *(std::vector<double>*)(m_PtResponse->Get(Form("TransferFunction_%s_%1.1f_%1.1f_ln(pT[GeV])", DetectPart[i], logPT[j],logPT[j+1])));
    }
  }
  
  // ------------- Parameter tensors Photon Resolution inizialization ----------------- //
  TaoTa_par_PhotonResol = *(std::vector<double>*)(m_PhotonResolution->Get("Photons_Resolution_TaOTa_params"));

  // ------------- Parameter tensors inizialization - pXpY constraint ----------------- //
  for(int i=0; i<4; i++){
    TaOTa_par_tensor_pXconstr[i]= *(std::vector<double>*)(m_pXconstraint->Get(Form("%d_AdditionalJets_pX",i)));
    TaOTa_par_tensor_pYconstr[i]= *(std::vector<double>*)(m_pYconstraint->Get(Form("%d_AdditionalJets_pY",i)));
  }

}
