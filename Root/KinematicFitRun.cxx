///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/
// KinematicFitRun.cxx
// Source file for class KinematicFitRun
// Magic happen here!
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
/////////////////////////////////////////////////////////////////// 


// KinematicFitRun includes
#include <AsgTools/MessageCheck.h>
#include "KinematicFitTool/KinematicFitRun.h"

class KinematicFitRun* gThis;

KinematicFitRun::KinematicFitRun() : asg::AsgTool ("KinematicFitRun"), Inc_Constr(0.), minuit(NULL)
{
  declareProperty("IncludingConstr",Inc_Constr);
}

KinematicFitRun::~KinematicFitRun() {

}


StatusCode KinematicFitRun::initialize() {

  return StatusCode::SUCCESS;
}

StatusCode KinematicFitRun::finalize() {
 

  return StatusCode::SUCCESS;
}

StatusCode KinematicFitRun::RunKF(KinematicFitEvent*& Event, KinematicFitResolution*& Resolution)
{

	NewEvent();

	_AddFitJets.resize(Event->GetAddJets().size());

	for(unsigned int i=0; i<Event->GetAddJets().size(); i++){                                                                                           
	  _AddFitJets[i].SetPtEtaPhiE(0.,0.,0.,0.);                                                                                                          
	}   

  	SetEventAndResolution(Event,Resolution);

	Run();

   	return StatusCode::SUCCESS; 
}

StatusCode KinematicFitRun::SetEventAndResolution(KinematicFitEvent*& Event, KinematicFitResolution*& Resolution)
{
	_Event = Event;
	_Resolution = Resolution; 
	return StatusCode::SUCCESS;
}

void KinematicFitRun::Run()
{
	gThis=this;
	isfitted = false;
	Int_t npar = GetNParameter();
	int conv = -99;
	if(minuit) delete minuit;
	minuit = new TMinuit(npar);
	conv = minuit->SetPrintLevel(-1);
	minuit->SetFCN(&KinematicFitRun::FCN);
	minuit->SetErrorDef(0.5);
	Double_t *arglist = new Double_t[npar];
	arglist[0] = 2;
	minuit->mnexcm("SET STR", arglist, 1, g);
	SetParameterNameandRange(minuit);
	arglist[0] = 50000.;
	arglist[1] = 1.;
	minuit->mnexcm("MIGRAD",arglist, 2, conv);
	isfitted = true;
	ShareFit(minuit);
	delete minuit;
	delete[] arglist;
}

void KinematicFitRun::SetParameterNameandRange(TMinuit*& m)
{

        m->mnparm(0, "EFit_photon1", _Event->GetPhotonOne()->e(), .01*_Resolution->PhotonRes()*_Event->GetPhotonOne()->e(), .1*_Resolution->PhotonRes()*_Event->GetPhotonOne()->e(), 2.*_Event->GetPhotonOne()->e(), g);
        m->mnparm(1, "EFit_photon2", _Event->GetPhotonTwo()->e(), .01*_Resolution->PhotonRes()*_Event->GetPhotonTwo()->e(), .1*_Resolution->PhotonRes()*_Event->GetPhotonTwo()->e(), 2.*_Event->GetPhotonTwo()->e(), g);

        m->mnparm(2, "EFit_jet1", _Event->GetBJetOne()->e(), .01*_Event->GetBJetOne()->e(), .1*_Event->GetBJetOne()->e(), 2.*_Event->GetBJetOne()->e(), g);
        m->mnparm(3, "EFit_jet2", _Event->GetBJetTwo()->e(), .01*_Event->GetBJetTwo()->e(), .1*_Event->GetBJetTwo()->e(), 2.*_Event->GetBJetTwo()->e(), g);

        // angular terms not included in KF but kept for possible other studies
        m->mnparm(4, "EtaFit_jet1", _Event->GetBJetOne()->eta(), .01*_Event->GetBJetOne()->eta(), -4.4, 4.4, g);
        m->mnparm(5, "EtaFit_jet2", _Event->GetBJetTwo()->eta(), .01*_Event->GetBJetTwo()->eta(), -4.4, 4.4, g);
        m->mnparm(6, "EtaFit_photon1", _Event->GetPhotonOne()->eta(), _Resolution->AnglesRes(), -7., 7., g);
        m->mnparm(7, "EtaFit_photon2", _Event->GetPhotonTwo()->eta(), _Resolution->AnglesRes(), -7., 7., g);

        m->mnparm(8, "PhiFit_jet1", _Event->GetBJetOne()->phi(), .01*_Event->GetBJetOne()->phi(), -1*TMath::Pi(), TMath::Pi(), g);
        m->mnparm(9, "PhiFit_jet2", _Event->GetBJetTwo()->phi(), .01*_Event->GetBJetTwo()->phi(), -1*TMath::Pi(), TMath::Pi(), g);
        m->mnparm(10, "PhiFit_photon1",_Event->GetPhotonOne()->phi(), _Resolution->AnglesRes(), -1*TMath::Pi(), TMath::Pi(), g);
        m->mnparm(11, "PhiFit_photon2",_Event->GetPhotonTwo()->phi(), _Resolution->AnglesRes(), -1*TMath::Pi(), TMath::Pi(), g);


	if(_Resolution->FixAngles() == true)
	{
		m->FixParameter(4);
		m->FixParameter(5);
		m->FixParameter(6);
		m->FixParameter(7);

		m->FixParameter(8);
		m->FixParameter(9);
		m->FixParameter(10);
		m->FixParameter(11);
	}

	if(_Event->GetAddJets().size() >= FitType::ThreeJet)
	  {
	    for(unsigned int i = 0; i < _Event->GetAddJets().size(); i++){
              m->mnparm(12+i*3, "EFit_jet"+std::to_string(i+3), _Event->GetAddJets().at(i)->e(), .01*_Event->GetAddJets().at(i)->e(), .1*_Event->GetAddJets().at(i)->e(), 2.*_Event->GetAddJets().at(i)->e(), g);
              m->mnparm(13+i*3, "EtaFit_jet"+std::to_string(i+3), _Event->GetAddJets().at(i)->eta(), .01*_Event->GetAddJets().at(i)->eta(), -4.4, 4.4, g);
              m->mnparm(14+i*3, "PhiFit_jet"+std::to_string(i+3), _Event->GetAddJets().at(i)->phi(), .01*_Event->GetAddJets().at(i)->phi(), -1*TMath::Pi(), TMath::Pi(), g);

	      if(_Resolution->FixAngles() == true)
		{
		  m->FixParameter(13+i*3);
		  m->FixParameter(14+i*3);
		}
	    }
	  }
}


double KinematicFitRun::GetLH()
{

	Double_t LH = 0., PxHH = 0., PyHH = 0.;

	// ------- Control Condition for Rare Nan Parameter cases ------- //                                          
	for(int i = 0; i < 4; i++){
	  if(std::isnan(_FitPhoton1[i]) || std::isnan(_FitPhoton2[i]) || std::isnan(_FitBJet1[i]) || std::isnan(_FitBJet2[i])){
	    return LH = std::numeric_limits<double>::infinity();
	  }
	  for(const auto& addfitJet : _AddFitJets){
	    if(std::isnan(addfitJet[i])){
	      return LH = std::numeric_limits<double>::infinity();
	    }
	  }
	}
	// ------- End Control condition ------- //

        LH += -2*log(GetTaOgataResponse(_FitBJet1, _Event->GetBJetOne(),"E"));
        LH += -2*log(GetTaOgataResponse(_FitBJet2, _Event->GetBJetTwo(),"E"));

        LH += -2*log(GetPhotonResolution(((_FitPhoton1.E() - _Event->GetPhotonOne()->e()) / _FitPhoton1.E())) );
        LH += -2*log(GetPhotonResolution(((_FitPhoton2.E() - _Event->GetPhotonTwo()->e()) / _FitPhoton2.E())) );

        LH += -2*log(GetTaOgataResponse(_FitBJet1, _Event->GetBJetOne(),"pT"));
        LH += -2*log(GetTaOgataResponse(_FitBJet2, _Event->GetBJetTwo(),"pT"));

        PxHH = (_FitBJet1 + _FitBJet2 + _FitPhoton1 + _FitPhoton2).Px();
        PyHH = (_FitBJet1 + _FitBJet2 + _FitPhoton1 + _FitPhoton2).Py();
	
	if(_Event->GetAddJets().size() >= FitType::ThreeJet)
	  {

	    for(unsigned int i = 0; i < _Event->GetAddJets().size(); i++){
              LH += -2*log(GetTaOgataResponse(_AddFitJets[i], _Event->GetAddJets().at(i),"E"));
              LH += -2*log(GetTaOgataResponse(_AddFitJets[i], _Event->GetAddJets().at(i),"pT"));

              PxHH += _AddFitJets[i].Px();
              PyHH += _AddFitJets[i].Py();
	    }
	  }

        LH += -2*lambda*log(GetMomConstraint(PxHH/1000,_Event->GetAddJets().size(),"pX"));
        LH += -2*lambda*log(GetMomConstraint(PyHH/1000,_Event->GetAddJets().size(),"pY"));

        if(Inc_Constr == 2){
          LH += lambda_m * pow((_FitBJet1+_FitBJet2).M()/1000 - 125., 2);
        }

	return LH;

}

void KinematicFitRun::SetParameters(double* par)
{
	
	_FitPhoton1.SetPtEtaPhiE(par[0]*sin(2*atan(exp(-1*par[6]))), par[6], par[10], par[0]);
	_FitPhoton2.SetPtEtaPhiE(par[1]*sin(2*atan(exp(-1*par[7]))), par[7], par[11], par[1]);
	_FitBJet1.SetPtEtaPhiE(par[2]*sin(2*atan(exp(-1*par[4]))), par[4], par[8], par[2]);
	_FitBJet2.SetPtEtaPhiE(par[3]*sin(2*atan(exp(-1*par[5]))), par[5], par[9], par[3]);
	for(unsigned int i = 0; i < _Event->GetAddJets().size(); i++){
	 
	  _AddFitJets[i].SetPtEtaPhiE(par[12+i*3]*sin(2*atan(exp(-1*par[13+i*3]))), par[13+i*3], par[14+i*3], par[12+i*3]);
	
	}
}

void KinematicFitRun::FCN(int& /*npar*/, double* /*grad*/, double& fval, double* par, int /*flag*/)
{

	
	gThis->SetParameters(par);
	
	fval = gThis->GetLH(); 
	
}

double KinematicFitRun::GetTaOgataResponse(TLorentzVector FitJet,  xAOD::Jet* Jet, const std::string& EorPT)
{
  return _Resolution->TaOgataResponse(FitJet, Jet, EorPT);
}

double KinematicFitRun::GetMomConstraint(double FitJetp, int nAddJets, const std::string& PxOrPy)
{
  return _Resolution->MomConstraint(FitJetp,nAddJets, PxOrPy);
}

double KinematicFitRun::GetPhotonResolution(double x)
{
  return _Resolution->Phot_Resolution(x);
}

void KinematicFitRun::ShareFit(TMinuit*& m)
{

	double eta, phi, E, err, pt;

	m->GetParameter(0,E,err);
	m->GetParameter(6,eta,err);
	m->GetParameter(10,phi,err);
	pt = E / cosh(eta);

	_FitPhoton1.SetPtEtaPhiE(pt,eta,phi,E);

	m->GetParameter(1,E,err);
	m->GetParameter(7,eta,err);
	m->GetParameter(11,phi,err);
	pt = E / cosh(eta);

	_FitPhoton2.SetPtEtaPhiE(pt,eta,phi,E);

	m->GetParameter(2,E,err);
	m->GetParameter(4,eta,err);
	m->GetParameter(8,phi,err);
	pt = E / cosh(eta);

	_FitBJet1.SetPtEtaPhiE(pt,eta,phi,E);

	m->GetParameter(3,E,err);
	m->GetParameter(5,eta,err);
	m->GetParameter(9,phi,err);
	pt = E / cosh(eta);

	_FitBJet2.SetPtEtaPhiE(pt,eta,phi,E);

	if(_Event->GetAddJets().size() >= FitType::ThreeJet)
	  {
	    for(unsigned int i=0; i<_Event->GetAddJets().size(); i++){

	      m->GetParameter(12+i*3,E,err);
	      m->GetParameter(13+i*3,eta,err);
	      m->GetParameter(14+i*3,phi,err);
	      pt = E / cosh(eta);

	      _AddFitJets[i].SetPtEtaPhiE(pt,eta,phi,E);
	    }
	  }
	
	_Event->SetFitPhotonOne(_FitPhoton1);
	_Event->SetFitPhotonTwo(_FitPhoton2);

	_Event->SetFitBJetOne(_FitBJet1);
	_Event->SetFitBJetTwo(_FitBJet2);

	_Event->SetFitAddJet(_AddFitJets);
}
void KinematicFitRun::NewEvent()
{

	_FitPhoton1.SetPtEtaPhiE(0., 0., 0., 0.);
	_FitPhoton2.SetPtEtaPhiE(0., 0., 0., 0.);
	_FitBJet1.SetPtEtaPhiE(0., 0., 0., 0.);
	_FitBJet2.SetPtEtaPhiE(0., 0., 0., 0.);
	_AddFitJets[0].SetPtEtaPhiE(0.,0.,0.,0.);

}

Int_t KinematicFitRun::GetNParameter()
{

	int n = 0;

	if(_Event->GetAddJets().size() == FitType::TwoJet)
	{	
		n = 12;
		
		if(_Resolution->FixAngles() == true)
		{
			n = 4;
		}

	} else if(_Event->GetAddJets().size() >= FitType::ThreeJet)
	  {
	    n = 12+(_Event->GetAddJets().size())*3;

	    if(_Resolution->FixAngles() == true)
	      {
		n = 4+(_Event->GetAddJets().size());
	      }
	  }
	
	return n;
}

Bool_t KinematicFitRun::isFitted()
{
	return isfitted;
}
