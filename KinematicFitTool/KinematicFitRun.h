///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/

// KinematicFitRun.h
// header file for class KinematicFitRun
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
///////////////////////////////////////////////////////////////////

#ifndef KinematicFitRun_H
#define KinematicFitRun_H 1

// KinematicFitRun includes
#include <string.h>
#include <TString.h>
#include <TEnv.h>
#include "AsgTools/AsgTool.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "TMinuit.h"
#include "KinematicFitTool/KinematicFitEvent.h"
#include "KinematicFitTool/KinematicFitResolution.h"

class KinematicFitRun : public asg::AsgTool{

public:

  /// Constructor with parameter name: 
  KinematicFitRun();

  /// Destructor: 
  virtual            ~KinematicFitRun(); 
  StatusCode         initialize();
  StatusCode         finalize();
  StatusCode RunKF(KinematicFitEvent*& Event, KinematicFitResolution*& Resolution);
  Bool_t isFitted();
protected:

	StatusCode SetEventAndResolution(KinematicFitEvent*& Event, KinematicFitResolution*& Resolution);
	void Run();
	void SetParameterNameandRange(TMinuit*& m);
	static void FCN(int& /*npar*/, double* /*grad*/, double& fval, double* par, int /*flag*/);
        double GetTaOgataResponse(TLorentzVector FitJet,  xAOD::Jet* Jet,const std::string& EorPT);
        double GetMomConstraint(double FitJetp,int nAddJets, const std::string& PxOrPy);
        double GetPhotonResolution(double x);

	Int_t GetNParameter();
	void NewEvent();
	void ShareFit(TMinuit*& m);
	double GetLH();
	void SetParameters(double* par);
	
private:

  Double_t Inc_Constr;
  enum FitType {TwoJet = 0, ThreeJet = 1};
  const int GeV     = 1e3;
  Int_t g = 2;
  Bool_t isfitted = false;
  Double_t lambda_m = 0.1;
  Double_t lambda = 3.05;
  TLorentzVector _FitPhoton1;
  TLorentzVector _FitPhoton2;
  TLorentzVector _FitBJet1;
  TLorentzVector _FitBJet2;
  std::vector< TLorentzVector > _AddFitJets = std::vector< TLorentzVector >(1);
  KinematicFitEvent* _Event;
  KinematicFitResolution* _Resolution;
  TMinuit *minuit;
  
 
}; 

#endif //> !KinematicFitRun_H
