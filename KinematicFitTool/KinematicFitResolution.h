///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/

// KinematicFitResolution.h
// header file for class KinematicFitResolution
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
///////////////////////////////////////////////////////////////////

#ifndef KinematicFitResolution_H
#define KinematicFitResolution_H 1


// KinematicFitResolution includes
#include <string.h>
#include <TString.h>
#include <TEnv.h>
#include "AsgTools/AsgTool.h"
#include <TH1F.h>
#include <TFile.h>
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"

class KinematicFitResolution : public asg::AsgTool {

public:

  /// Constructor with parameter name: 
  KinematicFitResolution();

  /// Destructor: 
  virtual            ~KinematicFitResolution(); 
  StatusCode initialize();
  StatusCode finalize();

  double PhotonRes();
  double AnglesRes();
  bool   FixAngles();
  double TaOgataResponse(TLorentzVector FitJet,  xAOD::Jet* Jet,const std::string& EorPT);
  double MomConstraint(double FitJetp, int nAddJets, const std::string& PxOrPy);
  double Phot_Resolution(double x);

  void import_par_tensor();

protected:
  
private:

private:

  //Variables for configuration

  std::vector<double> taOgata_par_tensor_E[4][6];
  std::vector<double> taOgata_par_tensor_pT[4][6];
  std::vector<double> TaoTa_par_PhotonResol;
  std::vector<double> TaOTa_par_tensor_pXconstr[4];
  std::vector<double> TaOTa_par_tensor_pYconstr[4];

  double m_photon_Res;
  double m_angles_Res;
  bool   m_isFixAngles;
  Double_t    m_Jet_Min_Pt;
  std::string m_BtaggingWP;
  TString m_EnergyResponseName;
  TString m_PtResponseName;
  TString m_PhotonResponseName;
  TString m_pXconstraintName;
  TString m_pYconstraintName;

  TFile* m_EnergyResponse;
  TFile* m_PtResponse;
  TFile* m_PhotonResolution;
  TFile* m_pXconstraint;
  TFile* m_pYconstraint;

  double taOgataRF(double x, std::vector<double> par){
    return par.at(8)*pow(TMath::ATan(par.at(0)*(x-par.at(1)))+TMath::Pi()/2,par.at(2))*exp(-pow((x-par.at(3))/par.at(4),2)/2)*pow(TMath::ATan(-par.at(5)*(x-par.at(6)))+TMath::Pi()/2,par.at(7));
  }

  double ExOata(double x, std::vector<double> par){
    return par.at(5)*exp(-exp(-par.at(0)*(x-par.at(1))))*pow(TMath::ATan(-par.at(2)*(x-par.at(3)))+TMath::Pi()/2,par.at(4));
  }

  double TaOTa(double x, std::vector<double> par){
    return par.at(6)*pow(TMath::ATan(par.at(0)*(x-par.at(1)))+TMath::Pi()/2,par.at(2))*pow(TMath::ATan(-par.at(3)*(x-par.at(4)))+TMath::Pi()/2,par.at(5));
  }

}; 

#endif //> !KinematicFitResolution_H
